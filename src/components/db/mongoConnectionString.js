"use strict";

const MONGO_PROTOCOL = "mongodb";

class MongoConnectionString {
  constructor(config) {
    this.host = config.host;
    this.port = config.port;
    this.dbName = config.name;
  }

  toString() {
    return `${MONGO_PROTOCOL}://${this.host}:${this.port}/${this.dbName}`;
  }
}

module.exports = MongoConnectionString;
