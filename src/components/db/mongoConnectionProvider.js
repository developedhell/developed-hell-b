"use strict";

const mongoose = require("mongoose");

const logger = require("../../helpers/get-logger")(__filename);

const MongoConnectionString = require("./mongoConnectionString");

class MongoConnectionProvider {
  constructor(config) {
    this.connectionString = new MongoConnectionString(config);
    this.mongoose = mongoose;
    this.mongoose.Promise = Promise;
  }

  async connect() {
    logger.info("Mongoose connection...");

    await this._connect();
  }

  async _connect() {
    await this.mongoose.connect(this.connectionString.toString(), {
      useMongoClient: true
    });
  }
}

module.exports = MongoConnectionProvider;
