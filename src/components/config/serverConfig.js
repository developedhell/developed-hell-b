"use strict";

const validator = require("validator");
const customValidator = require("../../helpers/validator");

const Config = require("./config");

module.exports = function createServerConfig() {
  return new Config({
    port: {
      value: () => {
        return process.env.HTTP_PORT;
      },
      validators: [validator.isPort]
    },
    host: {
      value: () => {
        return process.env.HTTP_HOST;
      },
      validator: [customValidator.isNotEmpty]
    },
    protocol: {
      value: () => {
        return process.env.HTTP_PROTOCOL;
      },
      validators: [customValidator.isHttpProtocol]
    }
  });
};
