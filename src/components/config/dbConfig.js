"use strict";

const validator = require("validator");
const customValidator = require("../../helpers/validator");

const Config = require("./config");

module.exports = function createServerConfig() {
  return new Config({
    port: {
      value: () => {
        return process.env.DB_PORT;
      },
      validators: [validator.isPort]
    },
    host: {
      value: () => {
        return process.env.DB_HOST;
      },
      validators: [customValidator.isNotEmpty]
    },
    name: {
      value: () => {
        return process.env.DB_NAME;
      },
      validators: [customValidator.isNotEmpty]
    }
  });
};
