"use strict";

const validator = require("validator");
const customValidator = require("../../helpers/validator");

const Config = require("./config");

module.exports = function createStaticConfig() {
  return new Config({
    port: {
      value: () => {
        return process.env.STATIC_PORT;
      },
      validators: [validator.isPort]
    },
    host: {
      value: () => {
        return process.env.STATIC_HOST;
      },
      validator: [customValidator.isNotEmpty]
    },
    protocol: {
      value: () => {
        return process.env.STATIC_PROTOCOL;
      },
      validators: [customValidator.isHttpProtocol]
    }
  });
};
