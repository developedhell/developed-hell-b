"use strict";

const EnvironmentVariablesLoader = require("../env/environmentVariablesLoader");

const createServerConfig = require("./serverConfig");
const createDbConfig = require("./dbConfig");
const createStaticConfig = require("./staticConfig");

class ConfigManager {
  constructor() {
    this._initEnvironmentVariables();

    this.dbConfig = this._createDbConfig();
    this.serverConfig = this._createServerConfig();
    this.staticConfig = this._createStaticConfig();
  }

  _initEnvironmentVariables() {
    const loader = new EnvironmentVariablesLoader();
    loader.load();
  }

  _createServerConfig() {
    return createServerConfig();
  }

  _createDbConfig() {
    return createDbConfig();
  }

  _createStaticConfig() {
    return createStaticConfig();
  }
}

module.exports = ConfigManager;
