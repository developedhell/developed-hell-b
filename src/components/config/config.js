"use strict";

class Config {
  constructor(configSchema) {
    if (!configSchema || typeof configSchema !== "object") {
      throw new Error("configSchema must be object");
    }

    this._configSchema = configSchema;

    this._initConfig();
  }

  _initConfig() {
    Object.keys(this._configSchema)
      .map(optionName => ({
        optionName,
        optionValue: this._getOptionValue(optionName)
      }))
      .map(({ optionName, optionValue }) => {
        this._validateOptionValue(optionName, optionValue);
        return { optionName, optionValue };
      })
      .forEach(({ optionName, optionValue }) => {
        this._defineOption(optionName, optionValue);
      });
  }

  _getOptionValue(optionName) {
    let value = null;
    if (!this._configSchema[optionName].value) {
      throw new Error("configSchema option must provide value");
    }
    if (typeof this._configSchema[optionName].value === "function") {
      value = this._configSchema[optionName].value();
    } else {
      value = this._configSchema[optionName].value;
    }
    return value;
  }

  _validateOptionValue(optionName, optionValue) {
    const validators = this._configSchema[optionName].validators || [];
    if (validators && !Array.isArray(validators)) {
      throw new Error("configSchema option validators must be array");
    }
    validators.forEach(validator => {
      if (typeof validator !== "function") {
        throw new Error("configSchema option validator must be a function");
      }
      if (!validator(optionValue)) {
        throw Error(`Option ${optionName} = ${optionValue} validation error`);
      }
    });
  }

  _defineOption(optionName, optionValue) {
    Object.defineProperty(this, optionName, {
      configurable: false,
      enumerable: true,
      writable: false,
      value: optionValue
    });
  }
}

module.exports = Config;
