"use strict";

const Subjects = require("./subjects");

const ACCESS_TOKEN_EXPIRATION = 60 * 60 * 24; // one day
const REFRESH_TOKEN_EXPIRATION = ACCESS_TOKEN_EXPIRATION + 60 * 60 * 24 * 60;
const EMAIL_CONFIRMATION_TOKEN_EXPIRATION = 60 * 60 * 3; // 3 hours
const RESTORE_PASSWORD_TOKEN_EXPIRATION = 60 * 60 * 3;

const ISSUER = "WTH";

let factory = null;

class TokenFactory {
  constructor(tokenService) {
    this.tokenService = tokenService;
  }

  async createAuthTokens(payload) {
    const expiresIn = Date.now() + ACCESS_TOKEN_EXPIRATION * 1000;

    const accessTokenOptions = {
      issuer: ISSUER,
      subject: Subjects.AUTHORIZATION,
      expiresIn: ACCESS_TOKEN_EXPIRATION
    };
    const refreshTokenOptions = {
      issuer: ISSUER,
      subject: Subjects.REFRESH_AUTHORIZATION,
      expiresIn: REFRESH_TOKEN_EXPIRATION,
      notBefore: ACCESS_TOKEN_EXPIRATION
    };
    const [accessToken, refreshToken] = await Promise.all([
      this.tokenService.generateToken(payload, accessTokenOptions),
      this.tokenService.generateToken(payload, refreshTokenOptions)
    ]);

    return { accessToken, refreshToken, expiresIn };
  }

  async createEmailConfirmationToken(payload) {
    const options = {
      issuer: ISSUER,
      subject: Subjects.EMAIL_CONFIRMATION,
      expiresIn: EMAIL_CONFIRMATION_TOKEN_EXPIRATION
    };

    const token = await this.tokenService.generateToken(payload, options);
    return token;
  }

  async createPasswordRestoreToken(payload) {
    const options = {
      issuer: ISSUER,
      subject: Subjects.PASSWORD_RESTORE,
      expiresIn: RESTORE_PASSWORD_TOKEN_EXPIRATION
    };

    const token = await this.tokenService.generateToken(payload, options);
    return token;
  }
}

module.exports = function getTokenFactory(tokenService) {
  if (factory === null) {
    factory = new TokenFactory(tokenService);
  }
  return factory;
};
