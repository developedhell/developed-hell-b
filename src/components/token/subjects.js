"use strict";

module.exports = {
  AUTHORIZATION: "authorization",
  REFRESH_AUTHORIZATION: "refreshAuthorization",
  EMAIL_CONFIRMATION: "emailConfirmation",
  PASSWORD_RESTORE: "passwordRestore"
};
