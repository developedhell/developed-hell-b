"use strict";

const dotenv = require("dotenv-safe");
const { resolve, join } = require("path");

const CONFIG_PATH = resolve(__dirname, "../../../config");
const ENV_FILE = ".env";
const EXAMPLE_FILE = ".env.example";
const DEFAULT_NODE_ENV = "development";

class EnvironmentVariableLoader {
  constructor() {
    this._isLoaded = false;
  }

  load() {
    if (!this._isLoaded) {
      dotenv.config({
        path: join(CONFIG_PATH, ENV_FILE),
        example: join(CONFIG_PATH, EXAMPLE_FILE)
      });

      process.env.NODE_ENV = process.env.NODE_ENV || DEFAULT_NODE_ENV;

      this._isLoaded = true;
    }
  }
}

module.exports = EnvironmentVariableLoader;
