"use strict";

module.exports = {
  MailSenderFactory: require("./mailSenderFactory"),
  EmailFactory: require("./emailFactory")
};
