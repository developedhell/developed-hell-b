const nodemailer = require("nodemailer");

class MailSender {
  constructor(config) {
    this.config = config;
    this.transporter = nodemailer.createTransport(config);
  }

  sendMail(mailOptions) {
    mailOptions.from = this.config.auth.user;
    return new Promise((resolve, reject) => {
      this.transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          return reject(error);
        }
        resolve(info);
      });
    });
  }
}

module.exports = MailSender;
