"use strict";

const url = require("url");
const util = require("util");

const DateUtils = require("../../helpers/date-utils");

const {
  TemplateService,
  TemplateTypes
} = require("../../services/templateService");

const EmailSubjects = {
  Confirmation: "Подтверждение почтового адреса",
  PasswordRestore: "Восстановление пароля"
};

const FORMAT_DATE_STRING = "%s.%s.%d %s:%s";

class EmailFactory {
  constructor(templateDir, tokenFactory, urlFactory) {
    this.templateService = new TemplateService(templateDir);
    this.tokenFactory = tokenFactory;
    this.urlFactory = urlFactory;
  }

  async createConfirmationEmail(data) {
    const token = await this.tokenFactory.createEmailConfirmationToken({
      userId: data.id
    });

    const confirmationLink = this.urlFactory.createConfirmationLinkUrl(token);

    const expirationDate = this._formatDateToString(
      DateUtils.addHours(new Date(), 3)
    );

    const html = this.templateService.render(TemplateTypes.EmailConfirmation, {
      nickname: data.nickname,
      confirmationLink,
      expirationDate
    });

    return {
      subject: EmailSubjects.Confirmation,
      html
    };
  }

  async createRestorePasswordEmail(data) {
    const token = await this.tokenFactory.createPasswordRestoreToken({
      userId: data.id
    });

    const restorationLink = this.urlFactory.createRestorePasswordLinkUrl(token);

    const expirationDate = this._formatDateToString(
      DateUtils.addHours(new Date(), 3)
    );

    const html = this.templateService.render(TemplateTypes.PasswordRestore, {
      nickname: data.nickname,
      restorationLink,
      expirationDate
    });

    return {
      subject: EmailSubjects.PasswordRestore,
      html
    };
  }

  _formatDateToString(date) {
    const day = DateUtils.addLeadingZero(date.getUTCDate());
    const month = DateUtils.addLeadingZero(date.getUTCMonth() + 1);
    const year = date.getUTCFullYear();
    const hours = DateUtils.addLeadingZero(date.getUTCHours());
    const minutes = DateUtils.addLeadingZero(date.getUTCMinutes());
    return util.format(FORMAT_DATE_STRING, day, month, year, hours, minutes);
  }
}

module.exports = EmailFactory;
