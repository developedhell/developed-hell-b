const MailSender = require("./mailSender");

let passwordRestoreMailSender = null;
let registrationConfirmMailSender = null;

class MailSenderFactory {
  constructor() {
    this._mailGatewayHost = process.env.MAIL_GATEWAY_HOST;
    this._mailGatewayPort = process.env.MAIL_GATEWAY_PORT;
    this._useSecure = this._mailGatewayPort == 465 ? true : false;
  }

  getPasswordRestoreMailSender() {
    if (passwordRestoreMailSender === null) {
      passwordRestoreMailSender = new MailSender({
        host: this._mailGatewayHost,
        port: this._mailGatewayPort,
        secure: this._useSecure,
        auth: {
          user: process.env.PASSWORD_MAIL_NAME,
          pass: process.env.PASSWORD_MAIL_PASSWORD
        }
      });
    }

    return passwordRestoreMailSender;
  }

  getRegistrationConfirmMailSender() {
    if (registrationConfirmMailSender === null) {
      registrationConfirmMailSender = new MailSender({
        host: this._mailGatewayHost,
        port: this._mailGatewayPort,
        secure: this._useSecure,
        auth: {
          user: process.env.REGISTRATION_MAIL_NAME,
          pass: process.env.REGISTRATION_MAIL_PASSWORD
        }
      });
    }

    return registrationConfirmMailSender;
  }
}

module.exports = MailSenderFactory;
