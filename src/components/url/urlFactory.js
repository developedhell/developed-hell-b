"use strict";

const UrlBuilder = require("./urlBuilder");

class UrlFactory {
  constructor(serverConfig) {
    this.serverConfig = serverConfig;
  }

  createConfirmationLinkUrl(token) {
    const linkUrl = new UrlBuilder(this.serverConfig);
    linkUrl.pathname = "/auth/confirm-email";
    linkUrl.searchParams.append("token", token);
    return linkUrl.toString();
  }

  createRestorePasswordLinkUrl(token) {
    const linkUrl = new UrlBuilder(this.serverConfig);
    linkUrl.pathname = "/user/restore-password";
    linkUrl.searchParams.append("token", token);
    return linkUrl.toString();
  }

  createLinkForUploadedFile(filename, prefix = "") {
    const linkUrl = new UrlBuilder(this.serverConfig);
    linkUrl.pathname = `/uploads/${
      prefix.length ? prefix + "/" : prefix
    }${filename}`;
    return linkUrl.toString();
  }
}

module.exports = UrlFactory;
