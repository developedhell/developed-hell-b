"use strict";

const url = require("url");

class UrlBuilder extends url.Url {
  constructor(config) {
    super();
    this.slashes = true;
    this.protocol = config.protocol;
    this.port = config.port;
    this.hostname = config.host;
    this.searchParams = new url.URLSearchParams();
  }

  toString() {
    this.search = this.searchParams.toString();
    return url.format(this);
  }
}

module.exports = UrlBuilder;
