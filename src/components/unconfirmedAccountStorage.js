"use strcict";

class UnconfirmedAccountStorage {
  constructor() {
    this.accounts = {};
  }

  saveAccount(type, account) {
    if (this.accounts[type] === undefined) {
      this.accounts[type] = {};
    }
    this.accounts[type][account.id] = account;
  }

  extractAccount(type, id) {
    const account = this.accounts[type] && this.accounts[type][id];
    this.accounts[type][id] = undefined;
    return account;
  }
}

module.exports = new UnconfirmedAccountStorage();
