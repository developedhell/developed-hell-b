"use strict";

module.exports = function requestuestSerializer(request) {
  return {
    method: request.method,
    url: request.url,
    headers: request.headers,
    query: request.query || null,
    body: request.body || null
  };
};
