"use strict";

module.exports = function responseponseSerializer(response) {
  return {
    status: response.status,
    length: response.length,
    type: response.type
  };
};
