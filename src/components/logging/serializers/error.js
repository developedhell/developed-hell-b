"use strict";

const bunyan = require("bunyan");

module.exports = bunyan.stdSerializers.err;
