"use strict";

module.exports = function serializeEmail(email) {
  return {
    accepted: email.accepted,
    rejected: email.rejected,
    from: email.envelope && email.envelope.from,
    type: email.type,
    messageTime: email.messageTime,
    messageSize: email.messageSize,
    messageId: email.messageId,
    response: email.response
  };
};
