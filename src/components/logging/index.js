"use strict";

const bunyan = require("bunyan");

const serializers = require("./serializers");

module.exports = bunyan.createLogger({
  name: "wth_backend",
  serializers
});
