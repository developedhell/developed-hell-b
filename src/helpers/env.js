const dotenv = require("dotenv-safe");
const { resolve, join } = require("path");

const CONFIG_PATH = resolve(__dirname, "../../config");
const ENV_FILE = ".env";
const EXAMPLE_FILE = ".env.example";

dotenv.config({
  path: join(CONFIG_PATH, ENV_FILE),
  example: join(CONFIG_PATH, EXAMPLE_FILE)
});

process.env.NODE_ENV = process.env.NODE_ENV || "production";
