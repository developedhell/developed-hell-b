"use strict";

const { promisify } = require("util");
const { randomBytes } = require("crypto");

module.exports = promisify(randomBytes);
