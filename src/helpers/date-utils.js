"use strict";

const MASK = "00";
const MASK_LENGTH = MASK.length;

const MS_IN_HOURS = 60 * 60 * 1000;

function fromHoursToMs(hours) {
  return hours * MS_IN_HOURS;
}

function addLeadingZero(n) {
  return (MASK + n).slice(-MASK_LENGTH);
}

function addHours(date, hours) {
  date.setTime(date.getTime() + fromHoursToMs(hours));
  return date;
}

module.exports = {
  fromHoursToMs,
  addLeadingZero,
  addHours
};
