"use strict";

const validator = require("validator");

const HTTP_PROTOCOL_PATTERN = /^https?$/;

function isHttpProtocol(protocol) {
  if (!protocol || typeof protocol !== "string") {
    throw new Error("Protocol must be a string");
  }
  return HTTP_PROTOCOL_PATTERN.test(protocol);
}

function isMinLength(length) {
  if (length < 0 && !isFinite(length)) {
    throw new Error("Min length must be finite number > 0");
  }
  return function(str) {
    return validator.isLength(str, { min: length });
  };
}

function isNotEmpty(str) {
  return !validator.isEmpty(str);
}

module.exports = {
  isHttpProtocol,
  isMinLength,
  isNotEmpty
};
