"use strict";

module.exports = function safeObjectAssign(target, ...sources) {
  target = typeof target === "object" ? target : {};
  return Object.assign(target, ...sources);
};
