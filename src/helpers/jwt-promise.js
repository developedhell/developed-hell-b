const promisify = require("util").promisify;

const jwt = require("jsonwebtoken");

module.exports = {
  sign: promisify(jwt.sign),
  verify: promisify(jwt.verify),
  decode: jwt.decode
};
