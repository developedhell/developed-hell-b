"use strict";

function getObjectValues() {
  if (typeof this !== "object") {
    throw new TypeError("this must be a object");
  }

  return Object.keys(this).map(key => this[key]);
}

function capitalize(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

function createEnumFromArray(array) {
  const result = Object.create(null);

  array.forEach(key => {
    Object.defineProperty(result, capitalize(key), {
      configurable: false,
      enumerable: true,
      writable: false,
      value: key
    });
  });

  return result;
}

function StringEnum(values) {
  if (!Array.isArray(values)) {
    throw new TypeError("types must be array of strings");
  }

  if (values.some(value => typeof value !== "string")) {
    throw new TypeError("types must be array of strings");
  }

  const stringEnum = createEnumFromArray(values);

  Object.defineProperty(stringEnum, "getValues", {
    configurable: false,
    enumerable: false,
    writable: false,
    value: getObjectValues
  });

  return Object.freeze(stringEnum);
}

module.exports = StringEnum;
