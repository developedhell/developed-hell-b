const Router = require("koa-router");

const initWelcomeRouter = require("./controllers/welcome");
const initAuthRouter = require("./controllers/auth");
const initUserRouter = require("./controllers/user");

function initRouter(passport) {
  const router = new Router();

  router.use("/", initWelcomeRouter().routes());
  router.use("/auth", initAuthRouter(passport).routes());
  router.use("/user", initUserRouter().routes());

  return router;
}

module.exports = initRouter;
