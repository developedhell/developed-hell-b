"use strict";

const path = require("path");

const Koa = require("koa");
const helmet = require("koa-helmet");
const validate = require("koa-validate");
const passport = require("koa-passport");
const staticHandler = require("koa-static");
const mount = require("koa-mount");

const logger = require("./helpers/get-logger")(__filename);

const handleError = require("./middlewares/errors");
const initAuth = require("./middlewares/auth");
const initCors = require("./middlewares/cors");
const jsonBodyParser = require("./middlewares/jsonBodyParser");
const logging = require("./middlewares/logging");

const BcryptPasswordService = require("./services/bcryptPasswordService");
const JwtTokenService = require("./services/jwtTokenService");
const MailService = require("./services/mailService");
const IdenticonService = require("./services/identiconService");
const { TemplateService } = require("./services/templateService");

const { ConfigManager } = require("./components/config");
const { MailSenderFactory, EmailFactory } = require("./components/mail");
const { MongoConfig, MongoConnectionProvider } = require("./components/db");
const { getTokenFactory } = require("./components/token");
const { UrlFactory } = require("./components/url");

const UserView = require("./views/user");

const initRouter = require("./router");

const TEMPLATE_DIR = path.resolve(__dirname, "../public/templates/mail");
const UPLOADS_DIR = path.resolve(__dirname, "../public/uploads");

class Application {
  constructor() {
    this.app = new Koa();
    this.configuration = new ConfigManager();
    this.db = null;
  }

  async _initDB() {
    this.db = new MongoConnectionProvider(this.configuration.dbConfig);
    await this.db.connect();
    logger.info("Mongoose connection successfully");
  }

  _initApp() {
    const tokenFactory = getTokenFactory(JwtTokenService);
    const urlFactory = new UrlFactory(this.configuration.serverConfig);

    this.app.env = process.env.NODE_ENV;
    this.app.keys = ["secret_key"]; // replace by strong key
    this.app.context.db = this.db;
    this.app.context.passwordService = BcryptPasswordService;
    this.app.context.tokenService = JwtTokenService;
    this.app.context.tokenFactory = tokenFactory;
    this.app.context.mailService = new MailService(
      new MailSenderFactory(),
      new EmailFactory(TEMPLATE_DIR, tokenFactory, urlFactory)
    );
    this.app.context.userView = new UserView(urlFactory);
    this.app.context.identiconService = new IdenticonService(
      path.join(UPLOADS_DIR, "avatars")
    );
    validate(this.app);
  }

  _initMiddlewares() {
    this.app.use(logging.initLogger);

    if (process.env.NODE_ENV === "development") {
      this.app.use(initCors());
    }

    this.app.use(helmet());
    this.app.use(handleError);

    this.app.use(jsonBodyParser);

    initAuth(passport);
    this.app.use(passport.initialize());

    this.app.use(logging.logRequest);
    this.app.use(logging.logResponse);

    if (process.env.NODE_ENV === "development") {
      // mount api doc
      this.app.use(
        mount("/doc", staticHandler(path.resolve(__dirname, "../doc")))
      );

      // mount uploads for dev env
      this.app.use(mount("/uploads", staticHandler(UPLOADS_DIR)));
    }
  }

  _initControllers() {
    const router = initRouter(passport);
    this.app.use(router.routes());
    this.app.use(router.allowedMethods());
  }

  async run() {
    await this._initDB();
    this._initApp();
    this._initMiddlewares();
    this._initControllers();

    const { HTTP_PORT } = process.env;
    this.app.listen(HTTP_PORT, () => {
      logger.info("Server started successfully on the port %s", HTTP_PORT);
    });
  }
}

module.exports = Application;

if (!module.parent) {
  const app = new Application();

  app
    .run()
    .then(() => logger.info("App successfully started..."))
    .catch(error => {
      logger.info("App started with error: %s", error.message);
      logger.info(error.stack);
      process.exit(1);
    });
}
