"use strict";

const mongoose = require("mongoose");

const MODEL_NAME = "User";

const User = new mongoose.Schema({
  nickname: {
    type: String,
    required: true,
    minlength: 4,
    maxlength: 64
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  passwordHash: {
    type: String
  },
  isEmailConfirmed: {
    type: Boolean,
    required: true,
    default: false
  },
  role: {
    type: String,
    enum: ["user", "moderator", "admin"],
    required: true,
    default: "user"
  },
  firstName: {
    type: String,
    minlength: 1,
    maxlength: 32
  },
  secondName: {
    type: String,
    minlength: 1,
    maxlength: 32
  },
  sex: {
    type: String,
    enum: ["male", "female"]
  },
  birthdate: {
    type: Date
  },
  country: {
    type: String,
    minlength: 3,
    maxlength: 32
  },
  city: {
    type: String,
    minlength: 2,
    maxlength: 32
  },
  avatar: {
    type: String,
    minlength: 1
  },
  vk: {
    id: {
      type: Number,
      index: true,
      unique: true,
      sparse: true
    },
    username: {
      type: String
    },
    displayName: {
      type: String
    },
    gender: {
      type: String
    },
    profileUrl: {
      type: String
    },
    photoUrl: {
      type: String
    }
  }
});

module.exports = mongoose.model(MODEL_NAME, User);
