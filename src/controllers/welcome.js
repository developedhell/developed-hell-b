const Router = require("koa-router");

async function welcome(ctx) {
  ctx.body = "Welcome";
  ctx.status = 200;
}

function initWelcomeRouter() {
  const router = new Router();

  router.get("/", welcome);

  return router;
}

module.exports = initWelcomeRouter;
