"use strict";

module.exports = {
  register: require("./register"),
  login: require("./login"),
  logout: require("./logout"),
  authBySocialNetwork: require("./authBySocialNetwork"),
  handleCallbackFromSocialNetwork: require("./handleCallbackFromSocialNetwork"),
  checkEmail: require("./checkEmail"),
  confirmAccount: require("./confirmAccount"),
  confirmEmail: require("./confirmEmail"),
  resendConfirmationEmail: require("./resendConfirmationEmail"),
  refreshToken: require("./refreshToken"),
  getAvatar: require("./getAvatar")
};
