"use strict";

const User = require("../../../models/user");

/**
 * @api {post} /auth/refresh-token Refresh accessToken
 *
 * @apiVersion 0.1.0
 *
 * @apiName RefreshToken
 * @apiGroup Auth
 *
 * @apiParam {string} refreshToken
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *         "refreshToken": "token"
 *     }
 *
 * @apiSuccess (200) {Object}  user Login user info
 * @apiSuccess (200) {string}  user.id
 * @apiSuccess (200) {string}  user.nickname
 * @apiSuccess (200) {string}  user.email
 * @apiSuccess (200) {string}  user.role
 * @apiSuccess (200) {boolean} user.isEmailConfirmed
 * @apiSuccess (200) {string}  user.[firstName]
 * @apiSuccess (200) {string}  user.[secondName]
 * @apiSuccess (200) {string}  user.[sex]
 * @apiSuccess (200) {string}  user.[birthdate]
 * @apiSuccess (200) {string}  user.[country]
 * @apiSuccess (200) {string}  user.[city]
 * @apiSuccess (200) {string}  user.[avatar] Url to user avatar
 * @apiSuccess (200) {Object}  tokens Authorizations tokens
 * @apiSuccess (200) {string}  tokens.accessToken Token for authorize
 * @apiSuccess (200) {string}  tokens.refreshToken
 *  Token for accessToken refreshing on expired
 * @apiSuccess (201) {number}  tokens.expiresIn
 *  Access token expiration time in ms
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 Created
 *     {
 *         "user": {
 *             "id": "5aacfe62b5159e89751ea9e8",
 *             "nickname": "qwerty",
 *             "email": "qwerty@ya.ru",
 *             "role": "user",
 *             "isEmailConfirmed": false
 *         },
 *         "tokens": {
 *             "accessToken": "token",
 *             "refreshToken": "token",
 *             "expiresIn": 1522164255492
 *         }
 *     }
 *
 *
 */
async function refreshToken(ctx) {
  const { refreshToken } = ctx.request.body;

  ctx.assert(refreshToken, 403, "Token was not provided");

  const verifyOptions = {
    subject: Subjects.REFRESH_AUTHORIZATION
  };

  const payload = await ctx.tokenService.verifyToken(
    refreshToken,
    verifyOptions
  );

  const user = await User.findById(payload.userId);

  ctx.assert(user, 403, "User not found");

  const tokens = await ctx.tokenFactory.createAuthTokens({
    userId: user.id
  });

  ctx.status = 201;

  ctx.body = {
    user: ctx.userView.render(user),
    tokens
  };
}

module.exports = refreshToken;
