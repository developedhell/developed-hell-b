"use strict";

/**
 * @api {get} /auth:type Authorize by social network
 *
 * @apiVersion 0.1.0
 *
 * @apiName SocialNetworkAuth
 * @apiGroup Auth
 *
 * @apiParam {string="vk"} type Social network type (vk, facebook, etc.)
 */
function authBySocialNetwork(passport) {
  return function authBySocialNetwork(ctx, next) {
    const type = ctx.params.type;
    return passport.authenticate(type)(ctx, next);
  };
}

module.exports = authBySocialNetwork;
