"use strict";

/**
 * @api {post} /auth/login Login
 *
 * @apiVersion 0.1.0
 *
 * @apiName Login
 * @apiGroup Auth
 *
 * @apiParam {string} login
 * @apiParam {string} password
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *         "login": "qwerty@ya.ru",
 *         "password": "123qwerty",
 *     }
 *
 * @apiSuccess (200) {Object}  user Login user info
 * @apiSuccess (200) {string}  user.id
 * @apiSuccess (200) {string}  user.nickname
 * @apiSuccess (200) {string}  user.email
 * @apiSuccess (200) {string}  user.role
 * @apiSuccess (200) {boolean} user.isEmailConfirmed
 * @apiSuccess (200) {string}  user.[firstName]
 * @apiSuccess (200) {string}  user.[secondName]
 * @apiSuccess (200) {string}  user.[sex]
 * @apiSuccess (200) {string}  user.[birthdate]
 * @apiSuccess (200) {string}  user.[country]
 * @apiSuccess (200) {string}  user.[city]
 * @apiSuccess (200) {string}  user.[avatar] Url to user avatar
 * @apiSuccess (200) {Object}  tokens Authorizations tokens
 * @apiSuccess (200) {string}  tokens.accessToken Token for authorize
 * @apiSuccess (200) {string}  tokens.refreshToken
 *  Token for accessToken refreshing on expired
 * @apiSuccess (201) {number}  tokens.expiresIn
 *  Access token expiration time in ms
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 Created
 *     {
 *         "user": {
 *             "id": "5aacfe62b5159e89751ea9e8",
 *             "nickname": "qwerty",
 *             "email": "qwerty@ya.ru",
 *             "role": "user",
 *             "isEmailConfirmed": false
 *         },
 *         "tokens": {
 *             "accessToken": "token",
 *             "refreshToken": "token",
 *             "expiresIn": 1522164255492
 *         }
 *     }
 *
 */
function login(passport) {
  return async function login(ctx, next) {
    return passport.authenticate("local", async function(error, user, info) {
      if (error) {
        console.log("error:", error);
        return next(error);
      }

      if (!user) {
        ctx.throw(401, "User doesn't exists");
      }

      const tokens = await ctx.tokenFactory.createAuthTokens({
        userId: user.id
      });

      ctx.status = 200;

      ctx.body = {
        user: ctx.userView.render(user),
        tokens
      };
    })(ctx, next);
  };
}

module.exports = login;
