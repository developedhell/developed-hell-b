"use strict";

/**
 * @api {post} /auth/logout Logout
 *
 * @apiVersion 0.1.0
 *
 * @apiName Logout
 * @apiGroup Auth
 *
 */
async function logout(ctx) {
  ctx.logout();
  ctx.redirect("/");
}

module.exports = logout;
