"use strict";

const validator = require("validator");

const sendConfirmationMail = require("../helpers/sendConfirmationMail");

const User = require("../../../models/user");

/**
 * @api {post} /auth/register Register user
 *
 * @apiName RegisterUser
 * @apiGroup Auth
 *
 * @apiVersion 0.1.0
 *
 * @apiParam {string{4..}}  nickname
 * @apiParam {string}       email
 * @apiParam {string{8..}}  password
 * @apiParam {string{8..}}  confirmationPassword
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *         "nickname": "qwerty",
 *         "email": "qwerty@ya.ru",
 *         "password": "123qwerty",
 *         "confirmationPassword": "123qwerty"
 *     }
 *
 * @apiSuccess (201) {Object}  user Created user info
 * @apiSuccess (201) {string}  user.id
 * @apiSuccess (201) {string}  user.nickname
 * @apiSuccess (201) {string}  user.email
 * @apiSuccess (201) {string}  user.role
 * @apiSuccess (201) {boolean} user.isEmailConfirmed
 * @apiSuccess (200) {string}  user.[firstName]
 * @apiSuccess (200) {string}  user.[secondName]
 * @apiSuccess (200) {string}  user.[sex]
 * @apiSuccess (200) {string}  user.[birthdate]
 * @apiSuccess (200) {string}  user.[country]
 * @apiSuccess (200) {string}  user.[city]
 * @apiSuccess (200) {string}  user.[avatar] Url to user avatar
 * @apiSuccess (201) {Object}  tokens Authorizations tokens
 * @apiSuccess (201) {string}  tokens.accessToken Token for authorize
 * @apiSuccess (201) {string}  tokens.refreshToken
 *  Token for accessToken refreshing on expired
 * @apiSuccess (201) {number}  tokens.expiresIn
 *  Access token expiration time in ms
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 201 Created
 *     {
 *         "user": {
 *             "id": "5aacfe62b5159e89751ea9e8",
 *             "nickname": "qwerty",
 *             "email": "qwerty@ya.ru",
 *             "role": "user",
 *             "isEmailConfirmed": false
 *         },
 *         "tokens": {
 *             "accessToken": "token",
 *             "refreshToken": "token",
 *             "expiresIn": 1522164255492
 *         }
 *     }
 */
async function register(ctx) {
  const {
    nickname = "",
    email = "",
    password = "",
    confirmationPassword
  } = ctx.request.body;

  ctx.assert(validator.isEmail(email), 422, "Invalid email format");
  ctx.assert(
    validator.isLength(nickname, { min: 4 }),
    422,
    "Invalid nickname format"
  );
  ctx.assert(
    validator.isLength(password, { min: 8 }),
    422,
    "Password is short"
  );
  ctx.assert(password === confirmationPassword, 422, "Passwords do not match");

  let user = await User.findOne({ email }).exec();

  ctx.assert(user === null, 409, "User already exists");

  const passwordHash = await ctx.passwordService.generateHash(password);

  const avatar = await ctx.identiconService.generateAvatar(email);

  user = await User.create({
    nickname,
    email,
    passwordHash,
    avatar
  });

  const tokens = await ctx.tokenFactory.createAuthTokens({
    userId: user.id
  });

  ctx.status = 201;

  ctx.body = {
    user: ctx.userView.render(user),
    tokens
  };

  sendConfirmationMail(user, ctx.mailService, ctx.state.logger);
}

module.exports = register;
