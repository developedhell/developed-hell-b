"use strict";

const validator = require("validator");

const User = require("../../../models/user");

const NicknameCounter = require("../helpers/nickNameCounter");

/**
 * @api {get} /auth/avatar Get avatar for nickname
 *
 * @apiVersion 0.1.0
 *
 * @apiName GetAvatar
 * @apiGroup Auth
 *
 * @apiParam {string{4..}}  nickname
 *
 * @apiSuccess (200) {string} nickname Nickname with nickname number
 * @apiSuccess (200) {string} avatar Avatar file in base64 encoding
 *
 * @apiSuccessExample {json} EmailExists-Example:
 *     HTTP/1.1 200 OK
 *     {
 *         "nickname": "qwerty#0001"
 *         "avatar": "iVBORw0KGgoAAAANSUhEUgAAASwA..."
 *     }
 */
async function getAvatar(ctx) {
  let { nickname = "" } = ctx.query;
  ctx.assert(
    validator.isLength(nickname, { min: 4 }),
    422,
    "Invalid nickname format"
  );
  const nicknameCounter = new NicknameCounter(User);
  const nicknameNumber = await nicknameCounter.getNicknameNumber(nickname);
  nickname += "#" + nicknameNumber;
  const avatar = await ctx.identiconService.generateAvatar(nickname);

  ctx.body = {
    nickname,
    avatar
  };

  ctx.status = 200;
}

module.exports = getAvatar;
