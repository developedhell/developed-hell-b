"use strict";

const { Subjects } = require("../../../components/token");

const User = require("../../../models/user");

/**
 * @api {get} /auth/confirm-email?token= Confirm email
 *
 * @apiVersion 0.1.0
 *
 * @apiName ConfirmEmail
 * @apiGroup Auth
 *
 * @apiParam {string} token Token for email confirmation
 */
async function confirmEmail(ctx) {
  const token = ctx.query.token;

  ctx.assert(token, 403, "Token was not provided");

  const verifyOptions = {
    subject: Subjects.EMAIL_CONFIRMATION
  };

  const payload = await ctx.tokenService.verifyToken(token, verifyOptions);

  const user = await User.findById(payload.userId);

  ctx.assert(user, 403, "User not found");
  ctx.assert(!user.isEmailConfirmed, 403, "User email already confirmed");

  user.isEmailConfirmed = true;
  await user.save();

  ctx.redirect("/");
}

module.exports = confirmEmail;
