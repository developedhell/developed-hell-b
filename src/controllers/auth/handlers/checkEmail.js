"use strict";

const validator = require("validator");

const User = require("../../../models/user");

/**
 * @api {get} /auth/check-email Check email existing
 *
 * @apiVersion 0.1.0
 *
 * @apiName CheckEmail
 * @apiGroup Auth
 *
 * @apiParam {string} email
 *
 * @apiSuccess (200) {boolean} emailExists
 *
 * @apiSuccessExample {json} EmailExists-Example:
 *     HTTP/1.1 200 OK
 *     {
 *         "emailExists": true
 *     }
 */
async function checkEmail(ctx) {
  const { email = "" } = ctx.query;
  ctx.assert(validator.isEmail(email), 422, "Invalid email format");

  const user = await User.findOne({ email }).exec();
  let emailExists = false;

  if (user !== null) {
    emailExists = true;
  }

  ctx.body = { emailExists };
  ctx.status = 200;
}

module.exports = checkEmail;
