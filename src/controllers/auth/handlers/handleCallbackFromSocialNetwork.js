"use strict";

function handleCallbackFromSocialNetwork(passport) {
  return function handleCallbackFromSocialNetwork(ctx, next) {
    return passport.authenticate("vk", function(error, user, info) {
      if (error) {
        return next(error);
      }

      if (info.needConfirmAccount) {
        return ctx.redirect("/"); // TODO: make a normal link for redirect
      }

      if (info.isNewUser) {
        ctx.status = 201;
        sendConfirmationMail(user, ctx.mailService);
      } else {
        ctx.status = 200;
      }

      ctx.body = ctx.userView.render(user);

      return ctx.login(user);
    })(ctx, next);
  };
}

module.exports = handleCallbackFromSocialNetwork;
