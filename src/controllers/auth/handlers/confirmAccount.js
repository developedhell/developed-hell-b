"use strict";

const validator = require("validator");

const AccountStorage = require("../../../components/unconfirmedAccountStorage");

const User = require("../../../models/user");

/**
 * @api {post} /auth/confirm-account Confirm account
 *
 * @apiVersion 0.1.0
 *
 * @apiName ConfirmAccount
 * @apiGroup Auth
 *
 */
async function confirmAccount(ctx) {
  const {
    email = "",
    password = "",
    accountType = "",
    accountId = ""
  } = ctx.request.body;

  ctx.assert(validator.isEmail(email), 422, "Invalid email format");

  const user = await User.findOne({ email }).exec();
  ctx.assert(user, 404, "User not found");

  const isValidPassword = await ctx.passwordService.validate(
    password,
    user.passwordHash
  );
  ctx.assert(isValidPassword, 403, "Invalid password");

  const account = AccountStorage.extractAccount(accountType, accountId);
  ctx.assert(account, 404, "Account not found");

  user[accountType] = account;

  await user.save();

  ctx.status = 200;

  ctx.body = ctx.userView.render(user);
}

module.exports = confirmAccount;
