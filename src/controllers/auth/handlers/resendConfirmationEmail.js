"use strict";

const sendConfirmationMail = require("../helpers/sendConfirmationMail");

const User = require("../../../models/user");

/**
 * @api {get} /auth/resend-email-confirmation Resend email confirmation
 *
 * @apiVersion 0.1.0
 *
 * @apiName ResendEmailConfirmation
 * @apiGroup Auth
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 204 No Content
 */
async function resendEmailConfirmation(ctx) {
  const userId = ctx.state.userId;

  const user = await User.findById(userId);

  ctx.assert(user, 401, "User not found");
  ctx.assert(!user.isEmailConfirmed, 403, "User email already confirmed");

  ctx.status = 204;

  sendConfirmationMail(user, ctx.mailService, ctx.state.logger);
}

module.exports = resendEmailConfirmation;
