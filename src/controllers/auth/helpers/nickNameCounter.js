"use strict";

const util = require("util");

const User = require("../../../models/user");

const MASK_CHAR = "0";

class NicknameCounter {
  constructor(userModel) {
    this.userModel = userModel;
  }

  async getNicknameNumber(nickname) {
    const nicknameCount = await this.userModel.count({ nickname }).exec();
    const nicknameNumber = nicknameCount + 1;
    const limits = this._limits();
    for (let limit of limits) {
      if (nicknameNumber < limit.value) {
        const mask = MASK_CHAR.repeat(limit.digits);
        return (mask + nicknameNumber).slice(-mask.length);
      }
    }
  }

  *_limits() {
    let value = 9999;
    let digits = 4;
    yield { value, digits };
    while (value < Number.MAX_SAFE_INTEGER) {
      value = value * 10 + 9;
      digits++;
      yield { value, digits };
    }
  }
}

module.exports = NicknameCounter;
