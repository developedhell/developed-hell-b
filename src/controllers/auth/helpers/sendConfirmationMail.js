"use strict";

function logEmail(info, logger) {
  if (logger) {
    logger.info({
      email: {
        ...info,
        type: "emailConfirmation"
      }
    });
  }
}

function sendConfirmationMail(user, mailService, logger) {
  process.nextTick(
    (user, mailService) => {
      mailService
        .sendConfirmationMail(user.email, user)
        .then(info => {
          logEmail(info, logger);
        })
        .catch(error => {
          if (logger) {
            logger.error({ error });
          }
        });
    },
    user,
    mailService,
    logger
  );
}

module.exports = sendConfirmationMail;
