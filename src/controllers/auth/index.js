"use strict";

const Router = require("koa-router");

const checkAuth = require("../../middlewares/checkAuth");

const handlers = require("./handlers");

function initAuthRouter(passport) {
  const router = new Router();

  router.post("/register", handlers.register);
  router.post("/login", handlers.login(passport));
  router.post("/logout", checkAuth, handlers.logout);

  router.get("/check-email", handlers.checkEmail);
  router.get("/confirm-email", handlers.confirmEmail);
  router.get(
    "/resend-email-confirmation",
    checkAuth,
    handlers.resendConfirmationEmail
  );
  router.get("/avatar", handlers.getAvatar);

  router.post("/confirm-account", handlers.confirmAccount);
  router.post("/refresh-token", handlers.refreshToken);

  router.get("/:type", handlers.authBySocialNetwork(passport));
  router.get(
    "/:type/callback",
    handlers.handleCallbackFromSocialNetwork(passport)
  );

  return router;
}

module.exports = initAuthRouter;
