"use strict";

const lodash = require("lodash");
const moment = require("moment");

const BIRTHDATE_STRING_FORMAT = "M/D/YYYY";

const PROFILE_FIELDS = [
  "nickname",
  "firstName",
  "secondName",
  "sex",
  "birthdate",
  "country",
  "city"
];

function getProfileFieldsFromBody(body) {
  body = body || {};
  return lodash.pick(body, PROFILE_FIELDS);
}

function getBirthdate(birthdate) {
  return moment(birthdate, BIRTHDATE_STRING_FORMAT, true);
}

module.exports = {
  getProfileFieldsFromBody,
  getBirthdate
};
