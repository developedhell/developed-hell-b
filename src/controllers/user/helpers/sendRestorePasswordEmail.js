"use strict";

function logEmail(info, logger) {
  if (logger) {
    logger.info({
      email: {
        ...info,
        type: "passwordRestore"
      }
    });
  }
}

function sendRestorePasswordEmail(user, mailService, logger) {
  process.nextTick(
    (user, mailService, logger) => {
      mailService
        .sendRestorePasswordMail(user.email, user)
        .then(info => {
          logEmail(info, logger);
        })
        .catch(error => {
          if (logger) {
            logger.error({ error });
          }
        });
    },
    user,
    mailService,
    logger
  );
}

module.exports = sendRestorePasswordEmail;
