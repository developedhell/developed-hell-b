"use strict";

const validator = require("validator");

const User = require("../../../models/user");

async function restorePassword(ctx) {
  const token = ctx.query.token;

  const { password = "", confirmationPassword } = ctx.request.body;

  ctx.assert(token, 403, "Token was not provided");
  ctx.assert(
    validator.isLength(password, { min: 8 }),
    422,
    "Password is short"
  );
  ctx.assert(password === confirmationPassword, 422, "Passwords do not match");

  const payload = await ctx.tokenService.verifyToken(token);
  ctx.assert(payload.type === "passwordRestore", 403, "Invalid token type");

  const user = await User.findById(payload.userId);
  ctx.assert(user, 403, "User not found");

  const passwordHash = await ctx.passwordService.generateHash(password);
  user.passwordHash = passwordHash;
  await user.save();

  ctx.redirect("/");
}

module.exports = restorePassword;
