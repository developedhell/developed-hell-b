"use strict";

const User = require("../../../models/user");

const {
  getBirthdate,
  getProfileFieldsFromBody
} = require("../helpers/profile");

/**
 * @api {put} /user Update user profile info
 *
 * @apiName UpdateUserProfileInfo
 * @apiGroup User
 *
 * @apiVersion 0.1.0
 *
 * @apiParam {string{4..64}}           [nickname]
 * @apiParam {string{1..32}}           [firstName]
 * @apiParam {string{1..32}}           [secondName]
 * @apiParam {string="male","female"}  [sex]
 * @apiParam {string{8..10}}           [birthdate] Birthdate in format M/D/YYYY
 * @apiParam {string{3..32}}           [country]
 * @apiParam {string{2..32}}           [city]
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *         "nickname": "qwerty",
 *         "firstName": "Bob",
 *         "secondName": "Martin",
 *         "sex": "male",
 *         "birthdate": "6/4/1990",
 *         "country": "Russia",
 *         "city": "Moscow"
 *     }
 *
 * @apiSuccess (200) {string}  id
 * @apiSuccess (200) {string}  nickname
 * @apiSuccess (200) {string}  email
 * @apiSuccess (200) {string}  role
 * @apiSuccess (200) {boolean} isEmailConfirmed
 * @apiSuccess (200) {string}  [firstName]
 * @apiSuccess (200) {string}  [secondName]
 * @apiSuccess (200) {string}  [sex]
 * @apiSuccess (200) {string}  [birthdate]
 * @apiSuccess (200) {string}  [country]
 * @apiSuccess (200) {string}  [city]
 * @apiSuccess (200) {string}  [avatar] Url to user avatar
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *         "id": "5ae4c96f0693aa64d80f47fb",
 *         "nickname": "qwerty",
 *         "email": "qwe123@ya.ru",
 *         "role": "user",
 *         "firstName": "Bob",
 *         "secondName": "Martin",
 *         "sex": "male",
 *         "birthdate": "6/4/1990",
 *         "country": "Russia",
 *         "city": "Moscow",
 *         "isEmailConfirmed": "true"
 *     }
 */
async function updateProfileInfo(ctx) {
  const userId = ctx.state.userId;
  const data = getProfileFieldsFromBody(ctx.request.body);

  if (data.birthdate) {
    const birthdate = getBirthdate(data.birthdate);
    ctx.assert(birthdate.isValid(), 422, "Birthdate invalid format");
    data.birthdate = birthdate.toDate();
  }

  const user = await User.findByIdAndUpdate(userId, data, {
    new: true,
    runValidators: true
  }).exec();

  ctx.body = ctx.userView.render(user);
  ctx.status = 200;
}

module.exports = updateProfileInfo;
