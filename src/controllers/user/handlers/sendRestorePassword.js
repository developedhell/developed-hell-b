"use strict";

const validator = require("validator");

const User = require("../../../models/user");

const sendRestorePasswordEmail = require("../helpers/sendRestorePasswordEmail");

async function sendRestorePassword(ctx) {
  const { email = "" } = ctx.request.body;
  ctx.assert(validator.isEmail(email), 422, "Invalid email format");

  let user = await User.findOne({ email }).exec();
  ctx.assert(user, 403, "User not found");

  ctx.status = 200;

  sendRestorePasswordEmail(user, ctx.mailService, ctx.state.logger);
}

module.exports = sendRestorePassword;
