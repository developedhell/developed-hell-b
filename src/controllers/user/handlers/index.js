"use strict";

module.exports = {
  sendRestorePassword: require("./sendRestorePassword"),
  restorePassword: require("./restorePassword"),
  updateProfileInfo: require("./updateProfileInfo"),
  uploadAvatar: require("./uploadAvatar")
};
