"use strict";

const path = require("path");

const User = require("../../../models/user");

async function uploadAvatar(ctx) {
  const userId = ctx.state.userId;
  const avatarFile = ctx.request.body.files.avatar;

  ctx.assert(avatarFile, 403, "Avatar file not provided");

  const avatar = path.basename(avatarFile.path);

  const user = await User.findByIdAndUpdate(
    userId,
    { avatar },
    {
      new: true,
      runValidators: true
    }
  ).exec();

  ctx.status = 200;
  ctx.body = ctx.userView.render(user);
}

module.exports = uploadAvatar;
