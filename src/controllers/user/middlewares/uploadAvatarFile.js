"use strict";

const path = require("path");

const avatarUploader = require("koa-body");

const MAX_AVATAR_SIZE = 10 * 1024 * 1024; // 10mb

const SUPPORTED_IMAGE_FORMATS = ["png", "gif", "jpeg", "jpg"];

const UPLOAD_DIR = path.resolve(
  __dirname,
  "../../../../",
  "public/uploads/avatars"
);

function constructImageTypePattern() {
  return (
    "image/" +
    SUPPORTED_IMAGE_FORMATS.map(format => {
      return `(${format})`;
    }).join("|")
  );
}

const IMAGE_TYPE_PATTERN = new RegExp(constructImageTypePattern(), "i");

// function filterFileType(file) {
//     const fileType = file.type;
//     if (!IMAGE_TYPE_PATTERN.test(fileType)) {
//         throw new Error('Unsupported file format');
//     }
// }

module.exports = avatarUploader({
  multipart: true,
  json: false,
  text: false,
  urlencoded: false,
  strict: true,
  formidable: {
    maxFields: 1,
    maxFieldsSize: MAX_AVATAR_SIZE,
    maxFileSize: MAX_AVATAR_SIZE,
    keepExtensions: true,
    multiples: false,
    uploadDir: UPLOAD_DIR
  },
  onError: (error, ctx) => {
    ctx.throw(`Avatar uploading error: ${error.message}`, 500);
  }
});
