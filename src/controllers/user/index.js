"use strict";

const Router = require("koa-router");

const checkAuth = require("../../middlewares/checkAuth");

const handlers = require("./handlers");
const internalMiddlewares = require("./middlewares");

function initUserRouter() {
  const router = new Router();

  router.post("/send-restore-password", handlers.sendRestorePassword);
  router.post("/restore-password", handlers.restorePassword);

  router.use("/", checkAuth);

  router.put("/", handlers.updateProfileInfo);
  router.put(
    "/avatar",
    internalMiddlewares.uploadAvatarFile,
    handlers.uploadAvatar
  );

  return router;
}

module.exports = initUserRouter;
