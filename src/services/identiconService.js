"use strict";

const fs = require("fs");
const path = require("path");
const crypto = require("crypto");

const jdenticon = require("jdenticon");

const randomBytesPromise = require("../helpers/random-bytes-promise");

const SUFFIX_SIZE = 16;

const BASE_AVATAR_NAME = "avatar";
const AVATAR_EXT = ".png";

const AVATAR_SIZE = 300;
const AVATAR_PADDING = 0.05;
const AVATAR_BACK_COLOR = "#FFF";

class IdenticonService {
  constructor(downloadsDir) {
    this._avatarGenerator = this._initAvatarGenerator();
    this._downloadsDir = downloadsDir;
  }

  async generateAvatar(value) {
    const avatar = this._avatarGenerator.toPng(
      value,
      AVATAR_SIZE,
      AVATAR_PADDING
    );
    return avatar.toString("base64");
  }

  async saveAvatar(avatar) {
    const avatarName = await this._getAvatarName();
    const avatarPath = path.join(this._downloadsDir, avatarName);
    const avatarData = Buffer.from(avatar, "base64");
    return new Promise((resolve, reject) => {
      fs.writeFile(avatarPath, avatarData, error => {
        if (error) {
          return reject(error);
        }
        resolve(avatarName);
      });
    });
  }

  _initAvatarGenerator() {
    jdenticon.config = {
      backColor: AVATAR_BACK_COLOR
    };
    return jdenticon;
  }

  async _getAvatarName() {
    const suffix = await randomBytesPromise(SUFFIX_SIZE);
    return BASE_AVATAR_NAME + "_" + suffix.toString("hex") + AVATAR_EXT;
  }
}

module.exports = IdenticonService;
