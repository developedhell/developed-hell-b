const jwt = require("../helpers/jwt-promise");
const assign = require("../helpers/safe-object-assign");

const SECRET = "secret"; // replace by strong secret

const defaultOptions = {
  algorithm: "HS256"
};

const defaultVerifyOptions = {
  algorithms: ["HS256"]
};

class TokenService {
  /**
   * Generate new JSON Web Token
   * @public
   * @static
   * @param {Object} payload
   * @returns {Promise<string>} token
   */
  static async generateToken(payload, options) {
    options = assign(options, defaultOptions);
    const token = await jwt.sign(payload, SECRET, options);
    return token;
  }

  static async verifyToken(token, options) {
    options = assign(options, defaultVerifyOptions);
    const payload = await jwt.verify(token, SECRET, options);
    return payload;
  }

  static getTime(interval) {
    return Math.floor(Date.now() / 1000) + interval;
  }
}

module.exports = TokenService;
