"use strict";

const path = require("path");
const fs = require("fs");

const pug = require("pug");

const StringEnum = require("../helpers/string-enum");

const TEMPLATE_EXT = ".ejs";

const EMAIL_TEMPLATES = ["emailConfirmation", "passwordRestore"];

const TemplateTypes = StringEnum(EMAIL_TEMPLATES);

class TemplateCache {
  constructor(templateDir) {
    if (!templateDir || typeof templateDir !== "string") {
      throw new Error("templateDir must be string");
    }

    if (!fs.existsSync(templateDir)) {
      throw new Error(`templateDir: <${templateDir}> doesn't exist`);
    }

    this._templateDir = templateDir;
    this._compliedTemplates = {};
  }

  init() {
    const files = fs.readdirSync(this._templateDir);
    files
      .filter(file => this._filterTemplates(file))
      .map(template => this._getFullTemplatePath(template))
      .forEach(template => {
        const templateName = this._getTemplateName(template);
        this._compliedTemplates[templateName] = this._compileTemplate(template);
      });
  }

  getCompiledTemplate(templateName) {
    if (!this._compliedTemplates[templateName]) {
      throw TypeError(`Unsuported template ${templateName}`);
    }
    return this._compliedTemplates[templateName];
  }

  _getFullTemplatePath(templateName) {
    return path.resolve(this._templateDir, templateName);
  }

  _filterTemplates(templateName) {
    const validExt = path.extname(templateName) === TEMPLATE_EXT;
    const validTemplate = EMAIL_TEMPLATES.some(t => {
      return t === this._getTemplateName(templateName);
    });
    return validExt && validTemplate;
  }

  _compileTemplate(template) {
    return pug.compileFile(template);
  }

  _getTemplateName(templatePath) {
    return path.basename(path.basename(templatePath, TEMPLATE_EXT));
  }
}

class TemplateService {
  constructor(templateDir) {
    this.templateDir = templateDir;
    this.templateCache = new TemplateCache(templateDir);
    this.templateCache.init();
  }

  render(templateName, data) {
    const template = this.templateCache.getCompiledTemplate(templateName);
    return template(data);
  }
}

module.exports = {
  TemplateService,
  TemplateTypes
};
