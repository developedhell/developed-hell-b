const bcrypt = require("bcrypt");

const ROUNDS = 10;

class BcryptPasswordService {
  /**
   * Generate password hash with bcrypt
   * @public
   * @static
   * @param {string} password
   * @returns {Promise<string>} passwordHash
   */
  static async generateHash(password) {
    const salt = await bcrypt.genSalt(ROUNDS);
    const passwordHash = await bcrypt.hash(password, salt);
    return passwordHash;
  }

  /**
   * Compare password and password hash with bcrypt
   * @public
   * @static
   * @param {string} password
   * @param {string} hash
   * @returns {Promise<boolean>} isValid
   */
  static async validate(password, hash) {
    const isValid = await bcrypt.compare(password, hash);
    return isValid;
  }
}

module.exports = BcryptPasswordService;
