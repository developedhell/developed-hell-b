const url = require("url");

class MailService {
  constructor(mailSenderFactory, emailFactory) {
    this.mailSenderFactory = mailSenderFactory;
    this.emailFactory = emailFactory;
  }

  async sendConfirmationMail(to, data) {
    const email = await this.emailFactory.createConfirmationEmail(data);

    const mailOptions = {
      to,
      ...email
    };

    return this.mailSenderFactory
      .getRegistrationConfirmMailSender()
      .sendMail(mailOptions);
  }

  async sendRestorePasswordMail(to, data) {
    const email = await this.emailFactory.createRestorePasswordEmail(data);

    const mailOptions = {
      to,
      ...email
    };

    return this.mailSenderFactory
      .getPasswordRestoreMailSender()
      .sendMail(mailOptions);
  }
}

module.exports = MailService;
