"use strict";

const lodash = require("lodash");
const moment = require("moment");

const BIRTHDATE_STRING_FORMAT = "M/D/YYYY";

const USER_PUBLIC_FIELDS = [
  "id",
  "nickname",
  "email",
  "role",
  "firstName",
  "secondName",
  "sex",
  "birthdate",
  "country",
  "city",
  "isEmailConfirmed",
  "avatar"
];

class UserView {
  constructor(urlFactory) {
    this.urlFactory = urlFactory;
  }

  render(user) {
    if (user && typeof user === "object") {
      const data = lodash.pick(user, USER_PUBLIC_FIELDS);
      if (data.birthdate) {
        const birthdate = moment(data.birthdate).format(
          BIRTHDATE_STRING_FORMAT
        );
        data.birthdate = birthdate;
      }
      if (data.avatar) {
        data.avatar = this.urlFactory.createLinkForUploadedFile(
          data.avatar,
          "avatars"
        );
      }
      return data;
    } else {
      return null;
    }
  }
}

module.exports = UserView;
