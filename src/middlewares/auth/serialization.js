const User = require("../../models/user");

function serialize(user, done) {
  return done(null, user.id);
}

function deserialize(id, done) {
  return User.findById(id, (error, user) => {
    if (error) {
      return done(error);
    }
    return done(null, user);
  });
}

module.exports = function initSerialization(passport) {
  passport.serializeUser(serialize);
  passport.deserializeUser(deserialize);
};
