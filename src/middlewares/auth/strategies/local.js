"use strict";

const LocalStrategy = require("passport-local").Strategy;

const User = require("../../../models/user");

const passwordService = require("../../../services/bcryptPasswordService");

async function verifyUser(email, password) {
  const user = await User.findOne({ email }).exec();
  if (!user) {
    return false;
  }

  const isValidPassword = await passwordService.validate(
    password,
    user.passwordHash
  );
  if (!isValidPassword) {
    return false;
  }

  return user;
}

module.exports = function initLocalAuthStrategy() {
  return new LocalStrategy(
    {
      usernameField: "login",
      passwordField: "password"
    },
    function localAuth(login, password, done) {
      verifyUser(login, password)
        .then(result => done(null, result))
        .catch(error => done(error));
    }
  );
};
