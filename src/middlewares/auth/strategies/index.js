"use strict";

const initLocalAuthStrategy = require("./local");
const initVkAuthStrategy = require("./vkontakte");

module.exports = function initStrategies(passport) {
  passport.use("local", initLocalAuthStrategy());
  passport.use("vk", initVkAuthStrategy());
};
