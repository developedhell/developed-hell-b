"use strict";

const url = require("url");

const VkontakteStrategy = require("passport-vkontakte").Strategy;

const AccountStorage = require("../../../components/unconfirmedAccountStorage");

const User = require("../../../models/user");

async function authUser(profile) {
  const info = { isNewUser: false, needConfirmAccount: false };

  let user = await User.findOne({ "vk.id": profile.id });
  if (user) {
    return { user, info };
  }

  user = await User.findOne({ email: profile.email });
  if (user) {
    AccountStorage.saveAccount("vk", account);
    info.needConfirmAccount = true;
    return { user, info };
  }

  user = await User.create({
    email: profile.email,
    nickname: profile.username,
    vk: profile
  });

  info.isNewUser = true;

  return { user, info };
}

const CALLBACK_URL = url.format({
  protocol: process.env.HTTP_PROTOCOL,
  hostname: process.env.HTTP_HOST,
  port: process.env.HTTP_PORT,
  pathname: "/auth/vk/callback"
});

module.exports = function initVkAuthStrategy() {
  return new VkontakteStrategy(
    {
      clientID: process.env.VK_CLIENT_ID,
      clientSecret: process.env.VK_CLIENT_SECRET,
      callbackURL: CALLBACK_URL,
      scope: ["email"],
      profileFields: ["email"],
      apiVersion: "5.17"
    },
    function(accessToken, refreshToken, params, profile, done) {
      profile.email = params.email;
      profile.photoUrl = profile.photos[0] && profile.photos[0].value;
      authUser(profile)
        .then(result => done(null, result.user, result.info))
        .catch(done);
    }
  );
};
