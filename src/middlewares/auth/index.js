const initSerialization = require("./serialization");
const initStrategies = require("./strategies");

module.exports = function initAuth(passport) {
  initSerialization(passport);
  initStrategies(passport);
};
