"use strict";

const bodyParser = require("koa-body");

module.exports = bodyParser({
  json: true,
  urlencoded: true,
  text: false,
  multipart: false,
  encoding: "utf-8",
  jsonLimit: "1mb",
  formLimit: "1mb",
  strict: true
});
