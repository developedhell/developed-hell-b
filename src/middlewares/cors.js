"use strict";

const cors = require("@koa/cors");

module.exports = function initCors() {
  const DAY = 86400; // day in second

  return cors({
    origin: "*",
    allowMethods: ["GET", "POST", "PUT", "DELETE"],
    allowHeaders: ["Authorization", "Content-Type"],
    maxAge: DAY,
    credentials: true
  });
};
