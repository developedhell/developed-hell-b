"use strict";

const MS_PER_SEC = 1000;
const MS_PER_NS = 1e-6;

function formatResponseTime(hrtime) {
  return hrtime[0] * MS_PER_SEC + hrtime[1] * MS_PER_NS;
}

module.exports = async function logResponse(ctx, next) {
  const requestTime = ctx.state.requestTime;
  const logger = ctx.state.logger;
  ctx.res.on("finish", () => {
    const responseTime = process.hrtime(requestTime);
    logger.info({
      response: ctx.response,
      responseTime: formatResponseTime(responseTime)
    });
  });
  return await next();
};
