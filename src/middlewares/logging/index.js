"use strict";

module.exports = {
  initLogger: require("./initLogger"),
  logRequest: require("./logRequest"),
  logResponse: require("./logResponse")
};
