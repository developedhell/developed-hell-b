"use strict";

const crypto = require("crypto");

const ID_LENGTH = 32;

module.exports = function generateRequestId() {
  return new Promise((resolve, reject) => {
    crypto.randomBytes(ID_LENGTH, (error, buffer) => {
      if (error) {
        return reject(error);
      }
      resolve(buffer.toString("hex"));
    });
  });
};
