"use strict";

const logger = require("../../components/logging");

const generateRequestId = require("./generateRequestId");

module.exports = async function initLogger(ctx, next) {
  ctx.state.requestTime = process.hrtime();
  const reqId = await generateRequestId();
  ctx.state.logger = logger.child({ reqId }, true);
  return await next();
};
