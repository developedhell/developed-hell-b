"use strict";

module.exports = async function logRequest(ctx, next) {
  ctx.state.logger.info({ request: ctx.request });
  return await next();
};
