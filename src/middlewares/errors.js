"use strict";

module.exports = async function handleError(ctx, next) {
  try {
    await next();
  } catch (error) {
    const { status, code, message } = error;
    const logger = ctx.state.logger;

    if (logger) {
      logger.error({ error });
    }

    ctx.status = status || 500;
    ctx.body = { code, message };
  }
};
