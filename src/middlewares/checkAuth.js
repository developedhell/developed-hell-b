"use strict";

const { Subjects } = require("../components/token");

function getToken(request) {
  const authHeader = request.headers["authorization"];
  if (!authHeader) {
    throw new Error("Authorization token was not provided");
  }

  const [schema, token] = authHeader.split(" ");
  if (!schema || schema !== "Bearer" || !token) {
    throw new Error("Bad authorization token format");
  }

  return token;
}

function handleAuthError(response, message) {
  response
    .status(401)
    .json({
      error: "AuthorizationError",
      message
    })
    .end();
}

async function checkAuth(ctx, next) {
  const verifyOptions = {
    subject: Subjects.AUTHORIZATION
  };

  try {
    const token = getToken(ctx.request);
    const payload = await ctx.tokenService.verifyToken(token, verifyOptions);
    ctx.state.userId = payload.userId;
    return await next();
  } catch (error) {
    ctx.throw(403, error.message, { error: "AuthorizationError" });
  }
}

module.exports = checkAuth;
