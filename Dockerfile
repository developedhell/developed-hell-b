FROM node:carbon

WORKDIR /app

COPY package*.json ./

RUN npm install
RUN npm run lint

COPY src ./src
COPY config ./config
COPY public ./public

COPY gulpfile.js ./
RUN npm run apidoc

EXPOSE 8088
CMD [ "node", "src/app.js" ]