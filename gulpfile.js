"use strict";

const path = require("path");

const gulp = require("gulp");
const apidoc = require("gulp-apidoc");

gulp.task("apidoc", function(done) {
  const APIDOC_CONFIG = path.resolve(__dirname, "./");
  const APIDOC_SRC = path.resolve(__dirname, "src/controllers");
  const APIDOC_DEST = path.resolve(__dirname, "doc");

  apidoc(
    {
      config: APIDOC_CONFIG,
      src: APIDOC_SRC,
      dest: APIDOC_DEST
    },
    done
  );
});
